This lab has a "Check stock" feature that parses XML input and returns any unexpected values in the response.

To solve the lab, inject an XML external entity to retrieve the contents of the /etc/passwd file.

https://portswigger.net/web-security/xxe/lab-exploiting-xxe-to-retrieve-files
